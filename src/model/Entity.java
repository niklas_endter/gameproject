package model;

public abstract class Entity {

    // BASIC PROPERTIES FOR THE MOVEMENT AND SIZE OF ANY DRAWN OBJECT IN THE GAME
    private float x;
    private float y;
    private float speedX = 0;
    private float speedY = 0;
    private int size = 48;

    public Entity(int x, int y, float speedX, float speedY, int size) {
        this.x = x;
        this.y = y;
        this.speedX = speedX;
        this.speedY = speedY;
        this.size = size;
    }

    //DETERMINE THE BEHAVIOUR OF OBJECTS IN THEIR SUBCLASS
    public void update(long elapsedTime) {
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSpeedX() {
        return speedX;
    }

    public void setSpeedX(float speedX) {
        this.speedX = speedX;
    }

    public float getSpeedY() {
        return speedY;
    }

    public void setSpeedY(float speedY) {
        this.speedY = speedY;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "x=" + x +
                ", y=" + y +
                ", speedX=" + speedX +
                ", speedY=" + speedY +
                ", size=" + size +
                '}';
    }
}
