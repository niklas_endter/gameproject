package model;

import javafx.scene.image.Image;

public class Enemy extends Entity{

    private Image[] images = new Image[3];
    private int live = 3;

    public Enemy(int x, int y, float speedX, float speedY, int size) {
        super(x, y, speedX, speedY, size);

        // ONE FOR EVERY LIVE STATUS
        images[2] = new Image("./resources/meteor.png", size, size, false, false);
        images[1] = new Image("./resources/meteor-2.png", size, size, false, false);
        images[0] = new Image("./resources/meteor-3.png", size, size, false, false);
    }

    public Image[] getImages() {
        return images;
    }

    public int getLive() {
        return live;
    }
    public void setLive(int live) {
        this.live = live;
    }
}
