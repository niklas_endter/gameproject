package model;

import javafx.scene.image.Image;

public class SpecialEnemy extends Enemy{

    private Image[] images = new Image[3];
    private int live = 3;

    public SpecialEnemy(int x, int y, float speedX, float speedY, int size) {
        super(x, y, speedX, speedY, size);
        images[0] = new Image("./resources/flare.png", size, size, false, false);
        images[1] = new Image("./resources/flare.png", size, size, false, false);
        images[2] = new Image("./resources/flare.png", size, size, false, false);
    }

    @Override
    public Image[] getImages() {
        return images;
    }

    public int getLive() {
        return live;
    }
    public void setLive(int live) {
        this.live = live;
    }

}
