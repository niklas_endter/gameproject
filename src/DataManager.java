import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataManager {

    private File data;
    private List<String> lines;

    public DataManager() {
        loadFile();
    }

    private void loadFile(){
        try {
            data = new File("data.txt");
            if (data.exists()){
                readFile();
            }else{
                boolean created = data.createNewFile();
                if (created)
                    readFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFile(){
        if (data.canRead()){
            try {
                FileReader fr = new FileReader(data);
                BufferedReader br = new BufferedReader(fr);
                lines = br.lines().toList(); // level_score = lines[level-1] -> FOR SIMPLICITY
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeLine(int level, int value){

        // NEWLINES REPRESENTING THE UPDATED SCORES
        List<String> newLines = new ArrayList<>();
        boolean wroteValue = false;
        for (int i = 0; i < lines.size(); i++){
            if (i == level-1) {
                if (Integer.parseInt(lines.get(i)) > value) {
                    newLines.add(String.valueOf(value));
                }else {
                    newLines.add(lines.get(i));
                }
                wroteValue = true;
            }else{
                newLines.add(lines.get(i));
            }
        }

        if (!wroteValue){
            newLines.add(String.valueOf(value));
        }

        this.lines = newLines;

        // RECREATE FILE AND WRITE EVERY LINE AGAIN BECAUSE YOU CAN'T DELETE ONE LINE WITH BUFFEREDWRITER
        boolean recreated = recreateFile();
        if (recreated){
            try {

                FileWriter fw = new FileWriter(data);
                BufferedWriter bw = new BufferedWriter(fw);
                for (String line : lines){
                    bw.write(line);
                    bw.newLine();
                }

                bw.close();
                fw.close();

                readFile();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean recreateFile(){
        boolean deleted = data.delete();
        if (deleted){
            try {
                data = new File("data.txt");
                boolean created = data.createNewFile();
                if (created)
                    return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public String getBestTimeOfLevel(int level){
        if (level > lines.size()){
            return "none";
        }else{
            return String.valueOf(lines.get(level-1));
        }
    }

}
