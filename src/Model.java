import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.*;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Model {

    // SCREEN SIZE
    public static final int WIDTH = 48*16; //768
    public static final int HEIGHT = 48*12; //576

    // SPAWN AREA
    private static final int SPAWN_AREA_ORIGIN_Y = -100;
    private static final int SPAWN_AREA_ORIGIN_X = -100;
    private static final int SPAWN_AREA_END_X = WIDTH+100;
    private static final int SPAWN_AREA_END_Y = HEIGHT+100;

    // OBJECTS
    private List<Enemy> enemies = new LinkedList<>();
    private List<Bullet> bullets = new LinkedList<>();
    private Player player;

    // GAME CYCLE
    private int level = 1;
    private int amount = 16;
    private boolean collided = false;
    private boolean survived = false;

    private int currentScore = 0;

    private Instant startTime;
    private int elapsedTime;

    private int speedMulti = 4;

    // DATA
    private DataManager dataManager;

    // SOUND
    private SoundManager soundManager;

    // MOVEMENT
    private boolean moveUp = false;
    private boolean moveDown = false;
    private boolean moveRight = false;
    private boolean moveLeft = false;


    // CONSTRUCTOR
    public Model() {

        // ADD ENEMIES
        spawnEnemies(amount);

        // ADD PLAYER
        this.player = new Player((WIDTH - 48)/2, (HEIGHT - 48)/2, 4);

        // ADD START-TIME
        this.startTime = Instant.now();

        // DATA
        dataManager = new DataManager();

        // SOUND
        soundManager = new SoundManager();

    }


    public void update(long elapsedTime) {

        if (!this.collided) {

            // UPDATE ENEMIES
            updateEnemies();

            // UPDATE PLAYER
            updatePlayer();

            // UPDATE BULLETS
            updateBullets();

            // UPDATE TIMER
            timer();

        }

        // COLLISIONS AND LEVEL MANAGEMENT
        this.collisionDetection();
        this.bulletCollisionDetection();
        this.hasSurvived();

    }


    // ENEMY GENERATION
    private void spawnEnemies(int amount){

        survived = false;
        collided = false;

        for (int i = 0; i < amount; i++){

            // DETERMINE DIRECTION - 1=DOWN 2=LEFT 3=UP 4=RIGHT
            int direction = generateRandomInt(1, 4);

            switch (direction){
                case 1:
                    generateDown("standard");
                    break;
                case 2:
                    generateLeft("standard");
                    break;
                case 3:
                    generateUp("standard");
                    break;
                case 4:
                    generateRight("standard");
                    break;
            }

        }

    }


    // GENERATE WITH DIRECTION
    private void generateDown(String enemyType){

        // SPAWN POINT
        int x = generateRandomInt(0, WIDTH);
        int y = generateRandomInt(-100, 0);

        // MOVING DIRECTION && SPEED - speedY > 0
        float leftGrad = angleOnePointToAnother(x, y, 0, 0);
        float rightGrad = angleOnePointToAnother(x, y, WIDTH, 0);

        float speedY = generateRandomFloat(0.5F, 2);
        float speedX = generateXSpeed(speedY, leftGrad, rightGrad);

        // SIZE
        int size = generateSize(Math.abs(speedX)+Math.abs(speedY));

        Enemy enemy;
        if (enemyType.equals("flaming")){
            enemy = new SpecialEnemy(x, y, 4, 4, 16);
        }else{
            enemy = new Enemy(x, y, speedX, speedY, size);
        }

        enemies.add(enemy);

    }

    private void generateLeft(String enemyType){

        // SPAWN POINT
        int x = generateRandomInt(WIDTH, WIDTH+100);
        int y = generateRandomInt(0, HEIGHT);

        // MOVING DIRECTION && SPEED
        float topGrad = angleOnePointToAnother(x, y, WIDTH, 0);
        float bottomGrad = angleOnePointToAnother(x, y, WIDTH, 0);

        float speedX = generateRandomFloat(-0.5F, -2);
        float speedY = generateYSpeed(speedX, topGrad, bottomGrad);

        // SIZE
        int size = generateSize(Math.abs(speedX)+Math.abs(speedY));

        Enemy enemy;
        if (enemyType.equals("flaming")){
            enemy = new SpecialEnemy(x, y, 4, 4, 16);
        }else{
            enemy = new Enemy(x, y, speedX, speedY, size);
        }

        enemies.add(enemy);

    }

    private void generateRight(String enemyType){

        // SPAWN POINT
        int x = generateRandomInt(-100, 0);
        int y = generateRandomInt(0, HEIGHT);

        // MOVING DIRECTION && SPEED
        float topGrad = angleOnePointToAnother(x, y, 0, 0);
        float bottomGrad = angleOnePointToAnother(x, y, 0, HEIGHT);

        float speedX = generateRandomFloat(0.5F, 2);
        float speedY = generateYSpeed(speedX, topGrad, bottomGrad);

        // SIZE
        int size = generateSize(Math.abs(speedX)+Math.abs(speedY));

        Enemy enemy;
        if (enemyType.equals("flaming")){
            enemy = new SpecialEnemy(x, y, 4, 4, 16);
        }else{
            enemy = new Enemy(x, y, speedX, speedY, size);
        }

        enemies.add(enemy);

    }

    private void generateUp(String enemyType){

        // SPAWN POINT
        int x = generateRandomInt(0, WIDTH);
        int y = generateRandomInt(HEIGHT, HEIGHT+100);

        // MOVING DIRECTION && SPEED
        float leftGrad = angleOnePointToAnother(x, y, 0, HEIGHT);
        float rightGrad = angleOnePointToAnother(x, y, WIDTH, HEIGHT);

        float speedY = generateRandomFloat(0.5F, 2);
        float speedX = generateXSpeed(speedY, leftGrad, rightGrad);

        // SIZE
        int size = generateSize(Math.abs(speedX)+Math.abs(speedY));

        Enemy enemy;
        if (enemyType.equals("flaming")){
            enemy = new SpecialEnemy(x, y, 4, 4, 16);
        }else{
            enemy = new Enemy(x, y, speedX, speedY, size);
        }

        enemies.add(enemy);

    }

    private float angleOnePointToAnother(int x1, int y1, int x2, int y2){
        int deltaX = x1-x2;
        int deltaY = y1-y2;
        return (float) deltaY/deltaX;
    }

    private float generateXSpeed(float speedY, float leftGrad, float rightGrad){
        float speedX = generateRandomFloat(-0.5F, 0.5F);
        //CHECK IF OBJECT WOULD FLY THROUGH SCREEN
        return speedX;
    }
    private float generateYSpeed(float speedX, float topGrad, float bottomGrad){
        float speedY = generateRandomFloat(-0.5F, 0.5F);
        //CHECK IF OBJECT WOULD FLY THROUGH SCREEN
        return speedY;
    }

    private int generateSize(float speed){
        // 1 - 2.5
        return (int) (64-(16*speed));
    }


    // ENEMIES UPDATE
    private void updateEnemies(){

        List<Enemy> deleteEnemies = new ArrayList<>();

        for (Enemy enemy: enemies){

            // MOVE
            enemy.setX(enemy.getX()+(enemy.getSpeedX()*speedMulti));
            enemy.setY(enemy.getY()+(enemy.getSpeedY()*speedMulti));

            // CHECK IF OUT OF BOUNDS
            if (enemy.getX() < SPAWN_AREA_ORIGIN_X ||
                enemy.getX() > SPAWN_AREA_END_X ||
                enemy.getY() < SPAWN_AREA_ORIGIN_Y ||
                enemy.getY() > SPAWN_AREA_END_Y){

                // DELETE ENEMY
                deleteEnemies.add(enemy);

            }

        }

        // DELETE OUT OF BOUNDS ENEMIES
        for (Enemy delete : deleteEnemies){
            enemies.remove(delete);
        }

    }


    // BULLETS UPDATE
    private void updateBullets(){

        List<Bullet> deleteBullets = new ArrayList<>();

        for (Bullet bullet: bullets) {
            // MOVE
            bullet.setY((int) (bullet.getY() + bullet.getSpeedY()));

            // CHECK IF OUT OF BOUNDS
            if (bullet.getX() < SPAWN_AREA_ORIGIN_X ||
                    bullet.getX() > SPAWN_AREA_END_X ||
                    bullet.getY() < SPAWN_AREA_ORIGIN_Y ||
                    bullet.getY() > SPAWN_AREA_END_Y){

                // DELETE ENEMIES
                deleteBullets.add(bullet);

            }

        }

        // DELETE OUT OF BOUNDS ENEMIES
        for (Bullet bullet : deleteBullets){
            bullets.remove(bullet);
        }

    }
    public void shoot(){
        Bullet bullet = new Bullet(
                (int) player.getX()+player.getSize()/2-6, // SPAWN IN THE MIDDLE OF THE SHIP
                (int) player.getY(),
                0,
                -4,
                12
        );
        bullets.add(bullet);

        soundManager.playLaser();

    }

    private void bulletCollisionDetection(){

        List<Bullet> deleteBullets = new ArrayList<>();
        List<Enemy> deleteEnemies = new ArrayList<>();

        // CHECK EVERY BULLET FOR EVERY ENEMY
        for (Bullet bullet : bullets){
            for (Enemy enemy : enemies){
                boolean collided = checkOneCollision(bullet, enemy);
                if (collided) {
                    enemy.setLive(enemy.getLive() - 1);
                    deleteBullets.add(bullet);

                    //SOUND MANAGER
                    soundManager.playHit();

                    if (enemy.getLive() < 1){
                        deleteEnemies.add(enemy);
                    }
                    break;
                }
            }
        }

        for (Bullet bullet : deleteBullets){
            this.bullets.remove(bullet);
        }
        for (Enemy enemy : deleteEnemies){

            // SCORE UPDATE
            int score;
            if (enemy instanceof SpecialEnemy) {
                // IF SPECIAL ENEMY 50 POINTS
                score = 50;
            } else{
                // CALCULATE A SCORE BASED ON THE SIZE / SPEED
                score = ((int) Math.ceil(Math.abs(enemy.getSpeedX()) + Math.abs(enemy.getSpeedY())));
            }
            currentScore += score;

            this.enemies.remove(enemy);
        }

    }

    // ENEMY COLLISION DETECTION
    private void collisionDetection(){
        for (Enemy enemy : enemies){
            boolean colliding = checkOneCollision(enemy, player);
            if (colliding) {
                if (!collided){
                    // SOUND
                    soundManager.playExplosion();
                }

                this.collided = true;

            }
        }
    }

    // CHECK COLLISION
    // SINCE THE CLASSES OF ALL OBJECTS THAT MAY COLLIDE INHERIT FROM ENTITY, I ONLY NEED ONE METHOD FOR CHECKING THAT
    private boolean checkOneCollision(Entity e, Entity e2){

        // CHECKING ANY OVERLAP

        int originEX = (int) e.getX();
        int endEX = originEX+e.getSize();
        int originE2X = (int) e2.getX();
        int endE2X = originE2X+e2.getSize();

        // CHECK X COLLISION
        if (!(originEX > originE2X && originEX < endE2X) &&
            !(endEX > originE2X && endEX < endE2X)){
            return false;
        }

        // CHECK Y COLLISION
        int originEY = (int) e.getY();
        int endEY = originEY+e.getSize();
        int originE2Y = (int) e2.getY();
        int endE2Y = originE2Y+e2.getSize();

        if (!(originEY > originE2Y && originEY < endE2Y) &&
            !(endEY > originE2Y && endEY < endE2Y)){
            return false;
        }

        // IF THERE IS NO COLLISION RETURN TRUE
        return true;

    }


    // CHECK IF SURVIVED
    private void hasSurvived(){
        if (enemies.size() == 0){
            this.survived = true;
            this.loadNextLevel();
        }
    }


    // HELPERS
    private int generateRandomInt(int min, int max){
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    private float generateRandomFloat(float min, float max){
        return (float) (min + (Math.random() * (max - min)));
    }



    // MOVEMENT
    public void setMoveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public void setMoveDown(boolean moveDown) {
        this.moveDown = moveDown;
    }

    public void setMoveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }

    public void setMoveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    private void updatePlayer() {

        float playerSpeed = player.getSpeedX();

        //KEYS
        if (moveUp){
            //MOVE UP
            player.movePlayer(0, -playerSpeed);
        }
        if (moveDown){
            //MOVE DOWN
            player.movePlayer(0, playerSpeed);
        }
        if (moveLeft){
            //MOVE LEFT
            player.movePlayer(-playerSpeed, 0);
        }
        if (moveRight){
            //MOVE RIGHT
            player.movePlayer(playerSpeed, 0);
        }

    }


    //SPECIAL ENEMIES
    private void spawnSpecials(int amount){

        for (int i = 0; i < amount; i++){

            // DETERMINE DIRECTION - 1=DOWN 2=LEFT 3=UP 4=RIGHT
            int direction = generateRandomInt(1, 4);

            switch (direction){
                case 1:
                    generateDown("flaming");
                    break;
                case 2:
                    generateLeft("flaming");
                    break;
                case 3:
                    generateUp("flaming");
                    break;
                case 4:
                    generateRight("flaming");
                    break;
            }

        }

    }


    // LEVELS
    private void loadNextLevel(){

        // SAVE NEW SCORE
        dataManager.writeLine(level, elapsedTime);

        //SOUND
        soundManager.playLevelUp();

        this.level++;
        this.amount+=8;

        this.spawnEnemies(this.amount);
        if (level > 2){
            this.spawnSpecials(level);
        }

        this.survived = false;
    }

    public void reset(){

        // RESET ALL NECESSARY PROPERTIES
        this.level = 1;
        this.amount = 16;
        this.currentScore = 0;
        this.startTime = Instant.now();
        this.bullets = new ArrayList<>();

        // RESET PLAYER IF OUT OF BOUNDS
        if ((this.player.getX() < 0 || this.player.getX() > WIDTH) ||
            (this.player.getY() < 0 || this.player.getY() > HEIGHT)) {
            this.player.setX((float) ((WIDTH - 48) / 2.0));
            this.player.setY((float) ((HEIGHT - 48) / 2.0));
        }

        this.enemies = new ArrayList<>();
        spawnEnemies(this.amount);

        this.collided = false;
    }

    private void timer(){
        Duration duration = Duration.between(this.startTime, Instant.now());
        this.elapsedTime = (int) duration.getSeconds();
    }

    public String getBestTimeForLevel(){
        return this.dataManager.getBestTimeOfLevel(this.level);
    }


    //GETTER AND SETTER
    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getSpeedMulti() {
        return speedMulti;
    }

    public void setSpeedMulti(int speedMulti) {
        this.speedMulti = speedMulti;
    }

    public List<Bullet> getBullets() {
        return bullets;
    }

    public int getLevel() {
        return level;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isCollided() {
        return collided;
    }

    public boolean isSurvived() {
        return survived;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public DataManager getDataManager() {
        return dataManager;
    }
}
