package model;

import javafx.scene.image.Image;

//import java.awt.*;

public class Player extends Entity{

    private Image image;

    public Player(int x, int y, int speed) {
        super(x, y, speed, speed, 48);
        image = new Image("./resources/ship_2.png", 48, 48, false, false);
    }

    public void movePlayer(float deltaX, float deltaY){
        setX(getX()+deltaX);
        setY(getY()+deltaY);
    }

    public Image getImage() {
        return image;
    }
}
