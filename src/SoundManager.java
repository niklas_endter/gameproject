import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class SoundManager {


    // SOUNDS
    private Media laser;
    private Media hit;
    private Media levelUp;
    private Media explosion;

    private Media background;
    private MediaPlayer backgroundPlayer;


    public SoundManager() {
        this.laser = new Media(new File("src/sounds/laser.wav").toURI().toString());
        this.hit = new Media(new File("src/sounds/hit.wav").toURI().toString());
        this.levelUp = new Media(new File("src/sounds/pickup.wav").toURI().toString());
        this.explosion = new Media(new File("src/sounds/explosion.wav").toURI().toString());

        this.background = new Media(new File("src/sounds/background.mp3").toURI().toString());
        this.backgroundPlayer = new MediaPlayer(background);
        this.backgroundPlayer.setVolume(.2);
        this.backgroundPlayer.play();
    }

    public void playLaser(){

        MediaPlayer mediaPlayer = new MediaPlayer(laser);
        mediaPlayer.setVolume(.6);
        mediaPlayer.play();

    }

    public void playHit(){

        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setVolume(.6);
        mediaPlayer.play();

    }

    public void playLevelUp(){

        MediaPlayer mediaPlayer = new MediaPlayer(levelUp);
        mediaPlayer.setVolume(.6);
        mediaPlayer.play();

    }

    public void playExplosion(){

        MediaPlayer mediaPlayer = new MediaPlayer(explosion);
        mediaPlayer.setVolume(.6);
        mediaPlayer.play();

    }

}
