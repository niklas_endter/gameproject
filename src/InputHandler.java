import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {

        // PLAYER MOVEMENT
        if (key == KeyCode.W || key == KeyCode.UP){
            model.setMoveUp(true);
        }
        if (key == KeyCode.S || key == KeyCode.DOWN){
            model.setMoveDown(true);
        }
        if (key == KeyCode.A || key == KeyCode.LEFT){
            model.setMoveLeft(true);
        }
        if (key == KeyCode.D || key == KeyCode.RIGHT){
            model.setMoveRight(true);
        }

        // FEATURES
        if (key == KeyCode.SHIFT){
            model.setSpeedMulti(1);
        }
        if (key == KeyCode.R){
            if (model.isCollided()){
                model.reset();
            }
        }

    }

    public void onKeyReleased(KeyCode key){

        // PLAYER MOVEMENT
        if (key == KeyCode.W || key == KeyCode.UP){
            model.setMoveUp(false);
        }
        if (key == KeyCode.S || key == KeyCode.DOWN){
            model.setMoveDown(false);
        }
        if (key == KeyCode.A || key == KeyCode.LEFT){
            model.setMoveLeft(false);
        }
        if (key == KeyCode.D || key == KeyCode.RIGHT){
            model.setMoveRight(false);
        }

        // FEATURES
        if (key == KeyCode.SHIFT){
            model.setSpeedMulti(4);
        }
        if (key == KeyCode.SPACE){
            model.shoot();
        }

    }
}
