# METEORID



## Goals

To complete every wave as fast as possible using the timer
OR
To reach the highest score by destroying the meteors with bullets.


## Controls

Use W, A, S, D or 'ArrowUp', 'ArrowLeft', 'ArrowDown', 'ArrowRight' to move your player.
Use 'Shift' to use slow motion. (Only slows down the meteors)
Use 'Space' to shoot.


## Assets
https://axassets.itch.io/spaceship-simple-assets
