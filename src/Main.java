import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javafx.scene.canvas.Canvas;

public class Main extends Application {

    private Timer timer;

    @Override
    public void start(Stage stage) throws Exception {

        //SETUP JAVAFX
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();

        Graphics graphics = new Graphics(model, gc);


        //INPUT HANDLER
        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );
        scene.setOnKeyReleased(
                event -> inputHandler.onKeyReleased(event.getCode())
        );

        timer = new Timer(model, graphics);
        timer.start();

    }

    @Override
    public void stop() throws Exception{
        timer.stop();
        super.stop();
    }

}
