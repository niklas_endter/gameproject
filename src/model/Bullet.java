package model;

import javafx.scene.image.Image;

public class Bullet extends Entity{

    private Image image;

    public Bullet(int x, int y, float speedX, float speedY, int size) {
        super(x, y, speedX, speedY, size);
        image = new Image("./resources/laser.png", size, size, false, false);
    }

    public Image getImage() {
        return image;
    }
}
