import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Bullet;
import model.Enemy;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {

        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        //DRAW BACKGROUND
        drawBackground();

        for (Enemy enemy : model.getEnemies()){

            //DRAW ENEMY
            drawEnemy(enemy);

        }

        for (Bullet bullet : model.getBullets()){

            //DRAW BULLET
            drawBullet(bullet);

        }

        //DRAW PLAYER
        this.drawPlayer();

        //DRAW LEVEL
        this.drawLevel();

        //DRAW SCORE
        this.drawScore();

        //DRAW TIMER
        this.drawTimer();

        //DRAW BEST TIME
        this.drawBestTime();

        //DRAW DIED
        this.drawDied();

    }

    private void drawPlayer(){

        gc.drawImage(model.getPlayer().getImage(), model.getPlayer().getX(), model.getPlayer().getY());

    }

    private void drawEnemy(Enemy enemy){

        gc.drawImage(enemy.getImages()[enemy.getLive()-1], enemy.getX(), enemy.getY());

    }

    private void drawBullet(Bullet bullet){

        gc.drawImage(bullet.getImage(), bullet.getX(), bullet.getY());

    }

    private void drawBackground(){

        if (model.getSpeedMulti() == 4){
            gc.setFill(Color.color(10/255.0, 10/255.0, 10/255.0, 1));
        }else{
            gc.setFill(Color.BLACK);
        }
        gc.fillRect(0, 0, Model.WIDTH, Model.HEIGHT);

    }

    private void drawDied(){

        if (model.isCollided()){
            gc.setFill(Color.WHITE);
            gc.setFont(new Font(36));
            gc.strokeText("PRESS R TO RESTART", 220, 300);
        }

    }

    private void drawScore(){

        gc.setStroke(Color.color(1, 1, 1, 0.5));
        gc.setFont(new Font(16));
        gc.strokeText("SCORE " + model.getCurrentScore(), 10, 60);

    }

    private void drawLevel(){

        gc.setStroke(Color.WHITE);
        gc.setFont(new Font(24));
        gc.strokeText("WAVE " + model.getLevel(), 10, 35);

    }

    private void drawTimer(){

        gc.setStroke(Color.WHITE);
        gc.setFont(new Font(24));
        gc.strokeText(String.valueOf(model.getElapsedTime()), Model.WIDTH-50, 35);

    }

    private void drawBestTime(){

        gc.setStroke(Color.color(1, 1, 1, 0.5));
        gc.setFont(new Font(16));
        gc.strokeText("BEST " + model.getBestTimeForLevel(), Model.WIDTH-75, 60);

    }

}
