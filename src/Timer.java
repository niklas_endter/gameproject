import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private long previousTime = -1;

    private Model model;
    private Graphics graphics;

    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long l) {

        long nowMilli = l/1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMilli - previousTime;
        }

        previousTime = nowMilli;


        // UPDATE MODEL
        model.update(elapsedTime);

        // DRAW OBJECTS
        graphics.draw();

    }

}
